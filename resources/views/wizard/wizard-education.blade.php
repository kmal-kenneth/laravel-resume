@extends('layouts.app')

@section('content')
<section class=" px-2">
    <div class="container">
        <div class="columns is-centered">
            <div class="column  ">

                <!-- <log-json json="{{ json_encode($educations) }} "></log-json> -->


                <app-stepper title="Education" steps="7" current-step="4" next-url="{{ route('wizard.personal') }} "
                    previous-url="{{ route('wizard.experience') }} ">
                    <div class="flex flex-wrap justify-center">

                        <form-education url-base="{{ route('wizard.education') }}"
                            educations-initial="{{ json_encode($educations) }}">
                        </form-education>
                    </div>
                </app-stepper>

            </div>
        </div>
    </div>
</section>



</div>
@endsection