@extends('layouts.app')

@section('content')
<section class=" px-2">
    <div class="container">
        <div class="columns is-centered">
            <div class="column  ">

                <!-- <log-json :json="erorrs"></log-json> -->


                <app-stepper title="Experience" steps="7" current-step="3" next-url="{{ route('wizard.education') }} "
                    previous-url="{{ route('wizard.photo') }} ">
                    <div class="flex flex-wrap justify-center">

                        <form-experience url-base="{{ route('wizard.experience') }}"
                            experiences-initial="{{ json_encode($experiences) }}">
                        </form-experience>
                    </div>
                </app-stepper>

            </div>
        </div>
    </div>
</section>



</div>


@endsection