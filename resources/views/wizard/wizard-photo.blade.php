@extends('layouts.app')

@section('content')
<section class=" px-2">
    <div class="container">
        <div class="columns is-centered">
            <div class="column  ">

                @if ($message = Session::get('success'))
                <app-message color="green-700">
                    <div>
                        <h2 class="text-xl font-semibold text-gray-800">
                            Success</h2>
                        <p class="text-gray-600">
                            {{ $message }}
                        </p>
                    </div>
                </app-message>
                @endif



                @if (count($errors) > 0)

                <app-message color="red-700">
                    <div>
                        <h2 class="text-xl font-semibold text-gray-800">
                            There were some problems with your input.
                        </h2>
                        <ul class="text-gray-600 list-disc ml-8">
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </app-message>
                @endif

                <app-stepper title=" Photography" steps="7" current-step="2"
                    next-url="{{ route('wizard.experience') }} " previous-url="{{ route('wizard.personal') }} ">

                    <form-photo action="{{ route('wizard.photo') }} " csrf-token="{{ csrf_token () }}"
                        image="/storage/avatars/{{ $user->avatar }}">
                    </form-photo>
                </app-stepper>

            </div>
        </div>
    </div>

</section>



</div>

@endsection