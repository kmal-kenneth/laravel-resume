@extends('layouts.app')

@section('content')
<section class=" px-2">
    <div class="container">
        <div class="columns is-centered">
            <div class="column  ">

                @if ($message = Session::get('success'))
                <app-message color="green-700">
                    <div>
                        <h2 class="text-xl font-semibold text-gray-800 ">
                            Success</h2>
                        <p class="text-gray-600">
                            {{ $message }}
                        </p>
                    </div>
                </app-message>
                @endif


                <app-stepper title=" Personal information" steps="7" current-step="1"
                    next-url="{{ route('wizard.photo') }} ">


                    <form-personal action="{{route('wizard.update', $user->id) }} " csrf-token="{{ csrf_token () }}"
                        email="{{ old('email') ? old('email') :  $user->email }}"
                        first_name="{{ old('first_name') ? old('first_name') :  $user->first_name }}"
                        last_name="{{ old('last_name') ? old('last_name') :  $user->last_name }}"
                        profession="{{ old('profession') ? old('profession') :  $user->profession }}"
                        profile="{{ old('profile') ? old('profile') :  $user->profile }}"
                        phone_number="{{ old('phone_number') ? old('phone_number') :  $user->phone_number }}"
                        web="{{ old('web') ? old('web') :  $user->web }}"
                        git_user="{{ old('git_user') ? old('git_user') :  $user->git_user }}"
                        address="{{ old('address') ? old('address') :  $user->address }}"
                        city="{{ old('city') ? old('city') :  $user->city }}"
                        country="{{ old('country') ? old('country') :  $user->country }}"
                        knowledge="{{ old('knowledge') ? old('knowledge') :  $user->knowledge }}"
                        birth_year="{{ old('birth_year') ? old('birth_year') :  $user->birth_year }}"
                        place_of_birth="{{ old('place_of_birth') ? old('place_of_birth') :  $user->place_of_birth }}"
                        citizen_id="{{ old('citizen_id') ? old('citizen_id') :  $user->citizen_id }}"
                        driver_license="{{ old('driver_license') ? old('driver_license') :  $user->driver_license }}"
                        email-error="@error('email') {{ $message }} @enderror"
                        first_name-error="@error('first_name') {{ $message }} @enderror"
                        last_name-error="@error('last_name') {{ $message }} @enderror">
                    </form-personal>


                </app-stepper>

            </div>
        </div>
    </div>
</section>



</div>



@endsection