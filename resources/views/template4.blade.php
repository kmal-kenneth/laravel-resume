@extends('layouts.app')

@section('content')

<div class="container">
    <button-take class="h-8"></button-take>
    <div class="w-full bg-gray-300 h-full flex justify-center py-2 px-1">
        <div id="resume" class="overflow-hidden py-12 px-10 bg-gray-900">
            <div class="flex justify-between">

                <div class="mb-8">
                    <div class="text-white font-bold text-xl font-sans">
                    {{{ $data['user']->first_name . " " . $data['user']->last_name }}}
                    </div>
                    <p class="text-sm text-gray-500 font-sans">
                    {{{ $data['user']->profession }}}
                    </p>
                </div>
                <img class="w-20 h-20 rounded-full mr-4" src="/storage/avatars/{{ Auth::user()->avatar }}"
                    alt="Your avatar">
            </div>
            <div class="">
                <label class="text-white text-base font-medium font-sans">ID:</label>
                <label class="text-gray-500 text-base font-sans">{{{ $data['user']->citizen_id }}}</label>
            </div>
            <div class="">
                <label class="text-white text-base font-medium font-sans">Date of birth:</label>
                <label class="text-gray-500 text-base font-sans">{{{ $data['user']->birth_year }}}</label>
            </div>
            <div class="">
                <label class="text-white text-base font-medium font-sans">Current address:</label>
                <label class="text-gray-500 text-base font-sans">
                {{{ $data['user']->country . ", " . $data['user']->city . ", " . $data['user']->address }}}
                </label>
            </div>
            <div class="">
                <label class="text-white text-base font-medium font-sans">Phone number:</label>
                <label class="text-gray-500 text-base font-sans">{{{ $data['user']->phone_number }}}</label>
            </div>
            <div class="mb-8">
                <label class="text-white text-base font-medium font-sans">Email:</label>
                <label class="text-gray-500 text-base font-sans">{{{ $data['user']->email }}}</label>
            </div>

            <div class="mb-6">
                <div class="text-white font-bold text-xl font-sans">
                    Formal education
                </div>
            </div>
            <div class="pl-4">
                @foreach ($data['educations'] as $education)
                    <div class="">
                        <label class="text-white text-base font-medium font-sans">{{$education->degree}}</label>
                    </div>
                    <div class="">
                        <label class="text-gray-500 text-base font-sans">{{$education->finish_month . ", " . $education->finish_year}}</label>
                    </div>
                    <div class="mb-4">
                        <label class="text-gray-600 text-base font-sans">{{$education->institution_name}}</label>
                    </div>
                @endforeach
            </div>

            <div class="mb-6">
                <div class="text-white font-bold text-xl font-sans">
                    Experience
                </div>
            </div>
            <div class="pl-4">
                @foreach ($data['experiences'] as $experience)
                    <div class="">
                        <label class="text-white text-base font-medium font-sans">{{$experience->company_name}}</label>
                    </div>
                    <div class="">
                        <label class="text-gray-500 text-base font-sans">{{$experience->position}}</label>
                    </div>
                    <div class="mb-4">
                        <label class="text-gray-600 text-base font-sans">
                        {{$experience->start_month . ", " . $experience->start_year . " to " .
                            $experience->finish_month . ", " . $experience->finish_year}}
                        </label>
                    </div>
                @endforeach
            </div>
            <div class="mb-6">
                <div class="text-white font-bold text-xl font-sans">
                    Expertise
                </div>
            </div>
            <ul class="list-disc pl-8">
                <li>
                    <div class="">
                        <label class="text-white text-base font-sans">B1 driver's license</label>
                        <label class="text-gray-500 text-base font-sans">100%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-white text-base font-sans">English.</label>
                        <label class="text-gray-500 text-base font-sans">95%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-white text-base font-sans">Windows & Microsoft Office</label>
                        <label class="text-gray-500 text-base font-sans">90%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-white text-base font-sans">Team work</label>
                        <label class="text-gray-500 text-base font-sans">100%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-white text-base font-sans">Groups management</label>
                        <label class="text-gray-500 text-base font-sans">90%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-white text-base font-sans">Speaking ease</label>
                        <label class="text-gray-500 text-base font-sans">95%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-white text-base font-sans">Work under pressure</label>
                        <label class="text-gray-500 text-base font-sans">100%</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

@endsection