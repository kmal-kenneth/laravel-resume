@extends('layouts.app')

@section('content')

<div class="container">
    <button-take class="h-8"></button-take>
    <div class="w-full bg-gray-300 h-full flex justify-center py-2 px-1">
        <div id="resume" class="overflow-hidden py-12 px-10 bg-gray-900">
            <div class="flex justify-center">
                <img class="w-20 h-20 rounded-full mr-4" src="/storage/avatars/{{ Auth::user()->avatar }}"
                        alt="Your avatar">
            </div>
            <div class="flex justify-center border border-yellow-500 p-3">
                <div class="">
                    <div class="text-yellow-500 font-bold text-xl flex justify-center font-sans">
                    {{{ $data['user']->first_name . " " . $data['user']->last_name }}}
                    </div>
                    <p class="text-sm text-blue-500 flex justify-center font-sans">
                    {{{ $data['user']->profession }}}
                    </p>
                </div>
            </div>
            <div class="border border-yellow-500 p-3">
                <div class="">
                    <label class="text-yellow-500 text-base font-medium font-sans">ID:</label>
                    <label class="text-blue-500 text-base font-sans">{{{ $data['user']->citizen_id }}}</label>
                </div>

                <div class="">
                    <label class="text-yellow-500 text-base font-medium font-sans">Date of birth:</label>
                    <label class="text-blue-500 text-base font-sans">{{{ $data['user']->birth_year }}}</label>
                </div>

                <div class="">
                    <label class="text-yellow-500 text-base font-medium font-sans">Current address:</label>
                    <label class="text-blue-500 text-base font-sans">
                    {{{ $data['user']->country . ", " . $data['user']->city . ", " . $data['user']->address }}}
                    </label>
                </div>

                <div class="">
                    <label class="text-yellow-500 text-base font-medium font-sans">Phone number:</label>
                    <label class="text-blue-500 text-base font-sans">{{{ $data['user']->phone_number }}}</label>
                </div>

                <div class="">
                    <label class="text-yellow-500 text-base font-medium font-sans">Email:</label>
                    <label class="text-blue-500 text-base font-sans">{{{ $data['user']->email }}}</label>
                </div>
            </div>

            <div class="border border-yellow-500 p-3">
                <div class="mb-6 flex justify-center">
                    <div class="text-yellow-500 font-bold text-xl font-sans">
                        Formal education
                    </div>
                </div>
                <div class="pl-4">
                    @foreach ($data['educations'] as $education)
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-medium font-sans">{{$education->degree}}</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">{{$education->finish_month . ", " . $education->finish_year}}</label>
                        </div>
                        <div class="mb-4 flex justify-center">
                            <label class="text-blue-500 text-base font-sans">{{$education->institution_name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="border border-yellow-500 p-3">
                <div class="mb-6 flex justify-center">
                    <div class="text-yellow-500 font-bold text-xl font-sans">
                        Experience
                    </div>
                </div>
                <div class="pl-4">
                    @foreach ($data['experiences'] as $experience)
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-medium font-sans">{{$experience->company_name}}</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">{{$experience->position}}</label>
                        </div>
                        <div class="mb-4 flex justify-center">
                            <label class="text-blue-500 text-base font-sans">
                            {{$experience->start_month . ", " . $experience->start_year . " to " .
                            $experience->finish_month . ", " . $experience->finish_year}}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="border border-yellow-500 p-3">
                <div class="mb-6 flex justify-center">
                    <div class="text-yellow-500 font-bold text-xl font-sans">
                        Expertise
                    </div>
                </div>
                <ul class="">
                    <li>
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-sans">B1 driver's license</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">100%</label>
                        </div>
                    </li>
                    <li>
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-sans">English</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">95%</label>
                        </div>
                    </li>
                    <li>
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-sans">Computer knowledge</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">75%</label>
                        </div>
                    </li>
                    <li>
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-sans">Team work</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">90%</label>
                        </div>
                    </li>
                    <li>
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-sans">Groups management</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">90%</label>
                        </div>
                    </li>
                    <li>
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-sans">Ease of dialogue</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">90%</label>
                        </div>
                    </li>
                    <li>
                        <div class="flex justify-center">
                            <label class="text-blue-800 text-base font-sans">Working under pressure</label>
                        </div>
                        <div class="flex justify-center">
                            <label class="text-blue-700 text-base font-sans">80%</label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

@endsection