@extends('layouts.app')

@section('content')

<div class="container">
    <button-take class="h-8"></button-take>
    <div class="w-full bg-gray-300 h-full flex justify-center py-2 px-1">
        <div id="resume" class="overflow-hidden py-12 px-10 bg-orange-400">
            <div class="flex justify-center">
                <img class="w-20 h-20 rounded-full mr-4" src="/storage/avatars/{{ Auth::user()->avatar }}"
                        alt="Your avatar">
            </div>
            <div class="flex justify-center">
                <div class="mb-8">
                    <div class="text-blue-900 font-bold text-xl flex justify-center font-mono">
                    {{{ $data['user']->first_name . " " . $data['user']->last_name }}}
                    </div>
                    <p class="text-sm text-blue-700 flex justify-center font-mono">
                    {{{ $data['user']->profession }}}
                    </p>
                </div>
            </div>
            <div class="flex justify-center">
                <label class="text-blue-900 text-base font-medium font-mono">ID:</label>
                <label class="text-blue-700 text-base font-mono">{{{ $data['user']->citizen_id }}}</label>
            </div>
            <div class="flex justify-center">
                <label class="text-blue-900 text-base font-medium font-mono">Date of birth:</label>
                <label class="text-blue-700 text-base font-mono">{{{ $data['user']->birth_year }}}</label>
            </div>
            <div class="flex justify-center">
                <label class="text-blue-900 text-base font-medium font-mono">Current address:</label>
                <label class="text-blue-700 text-base font-mono">
                {{{ $data['user']->country . ", " . $data['user']->city . ", " . $data['user']->address }}}
                </label>
            </div>
            <div class="flex justify-center">
                <label class="text-blue-900 text-base font-medium font-mono">Phone number:</label>
                <label class="text-blue-700 text-base font-mono">{{{ $data['user']->phone_number }}}</label>
            </div>
            <div class="mb-8 flex justify-center">
                <label class="text-blue-900 text-base font-medium font-mono">Email:</label>
                <label class="text-blue-700 text-base font-mono">{{{ $data['user']->email }}}</label>
            </div>

            <div class="mb-6 flex justify-center">
                <div class="text-blue-900 font-bold font-mono text-xl">
                    Formal education
                </div>
            </div>
            <div class="pl-4">
                @foreach ($data['educations'] as $education)
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-medium font-mono">{{$education->degree}}</label>
                    </div>
                    <div class="mb-4 flex justify-center">
                        <label class="text-blue-900 text-base font-mono">{{$education->finish_month . ", " . $education->finish_year}}</label>
                        <label class="text-blue-700 text-base font-mono">{{" | " . $education->institution_name}}</label>
                    </div>
                @endforeach
            </div>
            <div class="mb-6 flex justify-center">
                <div class="text-blue-900 font-bold font-mono text-xl">
                    Experience
                </div>
            </div>
            <div class="pl-4">
                @foreach ($data['experiences'] as $experience)
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-medium font-mono">{{$experience->company_name}}</label>
                    </div>
                    <div class="mb-4 flex justify-center">
                        <label class="text-blue-900 text-base font-mono">{{$experience->position}}</label>
                        <label class="text-blue-700 text-base font-mono">
                        {{" | " . $experience->start_month . ", " . $experience->start_year . " to " .
                            $experience->finish_month . ", " . $experience->finish_year}}
                        </label>
                    </div>
                @endforeach
            </div>
            <div class="mb-6 flex justify-center">
                <div class="text-blue-900 font-bold text-xl font-mono">
                    Expertise
                </div>

            </div>
            <ul class="">
                <li>
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-mono">B1 driver's license</label>
                        <label class="text-blue-700 text-base font-mono">100%</label>
                    </div>
                </li>
                <li>
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-mono">English.</label>
                        <label class="text-blue-700 text-base font-mono">95%</label>
                    </div>
                </li>
                <li>
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-mono">Windows & Microsoft Office</label>
                        <label class="text-blue-700 text-base font-mono">90%</label>
                    </div>
                </li>
                <li>
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-mono">Team work</label>
                        <label class="text-blue-700 text-base font-mono">100%</label>
                    </div>
                </li>
                <li>
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-mono">Groups management</label>
                        <label class="text-blue-700 text-base font-mono">90%</label>
                    </div>
                </li>
                <li>
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-mono">Speaking ease</label>
                        <label class="text-blue-700 text-base font-mono">95%</label>
                    </div>
                </li>
                <li>
                    <div class="flex justify-center">
                        <label class="text-blue-900 text-base font-mono">Work under pressure</label>
                        <label class="text-blue-700 text-base font-mono">100%</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>

</div>

@endsection