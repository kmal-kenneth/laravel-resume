@extends('layouts.app')

@section('content')
<section class=" px-2">
    <div class="container">
        <div class="columns is-centered">
            <div class="column  is-5-tablet ">
                <sign-up csrf-token="{{ csrf_token () }}" action="{{ route('register') }}" email="{{ old('email') }}"
                    email-error="@error('email') {{ $message }} @enderror"
                    password-error="@error('password') {{ $message }} @enderror" remember="{{ old('remember') }}"
                    password-request="@if (Route::has('password.request')) {{ route('password.request') }} @endif">
                </sign-up>
            </div>
        </div>
    </div>
</section>


@endsection