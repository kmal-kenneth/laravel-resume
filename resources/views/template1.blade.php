@extends('layouts.app')

@section('content')
<div class="container">
    <button-take class="h-8"></button-take>
    <div class="w-full bg-gray-300 h-full flex justify-center py-2 px-1">
        <div id="resume" class="overflow-hidden py-12 px-10 bg-white">
            <div class="flex justify-between items-center">
                <div class="">
                    <div class="text-gray-900 font-bold text-xl">
                        {{{ $data['user']->first_name . " " . $data['user']->last_name }}}
                    </div>
                    <p class="text-sm text-gray-700 flex items-center">
                        {{{ $data['user']->profession }}}
                    </p>
                </div>
                <img id="photo" class="w-24 h-24 rounded-full mr-4 " src="/storage/avatars/{{ Auth::user()->avatar }}"
                    alt="Your avatar" />
            </div>
            <div class="">
                <label class="text-gray-900 text-base font-medium">ID:</label>
                <label class="text-gray-700 text-base">{{{ $data['user']->citizen_id }}}</label>
            </div>
            <div class="">
                <label class="text-gray-900 text-base font-medium">Date of birth:</label>
                <label class="text-gray-700 text-base">{{{ $data['user']->birth_year }}}</label>
            </div>
            <div class="">
                <label class="text-gray-900 text-base font-medium">Current address:</label>
                <label class="text-gray-700 text-base">
                {{{ $data['user']->country . ", " . $data['user']->city . ", " . $data['user']->address }}}</label>
            </div>
            <div class="">
                <label class="text-gray-900 text-base font-medium">Phone number:</label>
                <label class="text-gray-700 text-base">{{{ $data['user']->phone_number }}}</label>
            </div>
            <div class="mb-8">
                <label class="text-gray-900 text-base font-medium">Email:</label>
                <label class="text-gray-700 text-base">{{{ $data['user']->email }}}</label>
            </div>

            <div class="mb-6">
                <div class="text-gray-900 font-bold text-xl">
                    Formal education
                </div>
            </div>
            <div class="pl-4">
                @foreach ($data['educations'] as $education)
                    <div class="">
                        <label class="text-gray-900 text-base font-medium">{{$education->degree}}</label>
                    </div>
                    <div class="mb-4">
                        <label class="text-gray-900 text-base">{{$education->finish_month . ", " . $education->finish_year}}</label>
                        <label class="text-gray-700 text-base">{{" | " . $education->institution_name}}</label>
                    </div>
                @endforeach
            </div>
            <div class="mb-6">
                <p class="text-gray-900 font-bold text-xl">
                    Experience
                </p>
            </div>
            <div class="pl-4">
                @foreach ($data['experiences'] as $experience)
                    <div class="">
                        <label class="text-gray-900 text-base font-medium">{{$experience->company_name}}</label>
                    </div>
                    <div class="">
                        <label class="text-gray-900 text-base">{{$experience->position}}</label>
                        <label class="text-gray-700 text-base">
                        {{" | " .$experience->start_month . ", " . $experience->start_year . " to " .
                            $experience->finish_month . ", " . $experience->finish_year}}</label>
                    </div>
                    <div class="mb-4">
                        <label class="text-gray-600 text-base">{{$experience->company_web}}</label>
                    </div>
                @endforeach
            </div>
            <div class="mb-6">
                <div class="text-gray-900 font-bold text-xl">
                    Expertise
                </div>
            </div>
            <ul class="list-disc pl-8">
                <li>
                    <div class="">
                        <label class="text-gray-900 text-base">B1 driver's license</label>
                        <label class="text-gray-700 text-base">100%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-gray-900 text-base">English.</label>
                        <label class="text-gray-700 text-base">95%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-gray-900 text-base">Windows & Microsoft Office</label>
                        <label class="text-gray-700 text-base">90%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-gray-900 text-base">Team work</label>
                        <label class="text-gray-700 text-base">100%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-gray-900 text-base">Groups management</label>
                        <label class="text-gray-700 text-base">90%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-gray-900 text-base">Ease of dialogue</label>
                        <label class="text-gray-700 text-base">95%</label>
                    </div>
                </li>
                <li>
                    <div class="">
                        <label class="text-gray-900 text-base">Work under pressure</label>
                        <label class="text-gray-700 text-base">100%</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

@endsection