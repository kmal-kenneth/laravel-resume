<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.5/dist/html2canvas.min.js"></script>


    <!-- Fonts -->
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>

<body class="min-h-screen bg-gray-300 has-navbar-fixed-top">
    <div id="app" class="">

        <header class="" data-html2canvas-ignore>
            <nav-bar class=" shadow-md container">

                <template slot="brand">
                    <a href="{{ url('/') }}" class="navbar-item is-uppercase">
                        <div class="container">
                            <div class=" title is-size-6 text-gray-900">Laravel</div>
                            <div class=" subtitle is-size-7 is-italic text-gray-800">Resume</div>
                        </div>
                    </a>
                </template>

                <template slot="start">

                </template>

                <template slot="end">
                    @auth

                    <div class="navbar-item has-dropdown is-hoverable">
                        <navbar-item url="{{route('wizard.personal') }}" text="Wizard" class="navbar-link">
                        </navbar-item>
                        <div class="navbar-dropdown">
                            <navbar-item text="Personal information" url="{{route('wizard.personal') }}"></navbar-item>
                            <navbar-item text="Photography" url="{{route('wizard.photo') }}"></navbar-item>
                            <navbar-item text="Experience" url="{{route('wizard.experience') }}">
                            </navbar-item>
                            <navbar-item text="Education" url="{{route('wizard.education') }}">
                            </navbar-item>
                            <navbar-item text="Template" url="{{route('wizard.template') }}">
                            </navbar-item>
                        </div>
                    </div>
                    @endauth

                    @guest

                    <div class="buttons">
                        <div class="navbar-item">
                            <a class="button is-light" href="{{ route('login') }}">
                                Log in
                            </a>
                        </div>

                        @if (Route::has('register'))

                        <div class="navbar-item">
                            <a class="button is-primary" href="{{ route('register') }}">
                                <div class="strong">
                                    Sign up
                                </div>
                            </a>
                        </div>

                        @endif
                    </div>

                    @else

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a href="" class="navbar-link flex items-center">
                            <img class="rounded-md shadow-md border border-gray-300 antialiased mr-2 h-8 w-8 object-cover"
                                src="/storage/avatars/{{ Auth::user()->avatar }}">
                            {{ Auth::user()->first_name . " " . Auth::user()->last_name}}
                        </a>
                        <div class="navbar-dropdown">
                            <a class="navbar-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>

                    @endguest

                </template>

            </nav-bar>
        </header>

        <main class=" main mt-3">


            @yield('content')
        </main>
    </div>




</body>





</html>