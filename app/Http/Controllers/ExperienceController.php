<?php

namespace App\Http\Controllers;

use App\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExperienceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $experiences = Experience::orderBy("finish_year", "DESC")->all();

        return view(
            'wizard.wizard-experience',
            compact('experiences', $experiences)
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wizard()
    {
        //

        $experiences = Experience::orderBy("finish_year", "desc")->get();

        return view(
            'wizard.wizard-experience',
            compact('experiences', $experiences)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'start_year' => 'required',
            'finish_year' => 'required',
            'position' => 'required',
            'company_name' => 'required',
        ]);

        $experience = new Experience();
        $experience->start_year = $request->start_year;
        $experience->start_month = $request->start_month;
        $experience->finish_year = $request->finish_year;
        $experience->finish_month = $request->finish_month;
        $experience->position = $request->position;
        $experience->company_name = $request->company_name;
        $experience->company_web = $request->company_web;
        $experience->job_description = $request->job_description;
        $experience->user_id = auth()->id();
        $experience->save();

        return $experience;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function edit(Experience $experience)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'start_year' => 'required',
            'finish_year' => 'required',
            'position' => 'required',
            'company_name' => 'required',
        ]);

        $experience = Experience::find($id);
        $experience->start_year = $request->start_year;
        $experience->start_month = $request->start_month;
        $experience->finish_year = $request->finish_year;
        $experience->finish_month = $request->finish_month;
        $experience->position = $request->position;
        $experience->company_name = $request->company_name;
        $experience->company_web = $request->company_web;
        $experience->job_description = $request->job_description;
        $experience->user_id = auth()->id();
        $experience->save();

        return $experience;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $experience = Experience::find($id);
        $experience->delete();
    }
}
