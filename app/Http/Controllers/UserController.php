<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = auth()->user();

        return view('wizard.wizard-personal', compact('user'));
    }

    /**
     * Display the auth resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wizard()
    {
        //
        $user = Auth::user();

        return view('wizard.wizard-personal', compact('user', $user));
    }

    /**
     * Display the auth resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wizard_photo()
    {
        //
        $user = Auth::user();

        return view('wizard.wizard-photo', compact('user', $user));
    }

    public function update_avatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = Auth::user();

        $avatarName =
            $user->id .
            '_avatar' .
            time() .
            '.' .
            request()->avatar->getClientOriginalExtension();

        $request->avatar->storeAs('avatars/', $avatarName);

        $oldAvatar = 'avatars/' . $user->avatar;
        if ($oldAvatar != "avatars/user.jpg") {
            Storage::delete($oldAvatar);
        }

        $user->avatar = $avatarName;
        $user->save();

        return back()->with('success', 'You have successfully upload image.');
    }

    /**
     * Display the auth resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_education()
    {
        //

        return view('wizard.wizard-education');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $user = Auth::user();
        $user->email = $request->get('email');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->profession = $request->get('profession');
        $user->profile = $request->get('profile');
        $user->phone_number = $request->get('phone_number');
        $user->web = $request->get('web');
        $user->git_user = $request->get('git_user');
        $user->city = $request->get('city');
        $user->country = $request->get('country');
        $user->knowledge = $request->get('knowledge');
        $user->birth_year = $request->get('birth_year');
        $user->place_of_birth = $request->get('place_of_birth');
        $user->citizen_id = $request->get('citizen_id');
        $user->driver_license = $request->get('driver_license');
        $user->address = $request->get('address');
        $user->save();

        return back()->with('success', 'Information updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
