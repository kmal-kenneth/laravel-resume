<?php

namespace App\Http\Controllers;

use App\Education;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wizard()
    {
        //

        $educations = Education::orderBy("finish_year", "desc")->get();

        return view(
            'wizard.wizard-education',
            compact('educations', $educations)
        );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'start_year' => 'required',
            'finish_year' => 'required',
            'degree' => 'required',
            'institution_name' => 'required',
        ]);

        $education = new Education();
        $education->start_year = $request->start_year;
        $education->start_month = $request->start_month;
        $education->finish_year = $request->finish_year;
        $education->finish_month = $request->finish_month;
        $education->degree = $request->degree;
        $education->institution_name = $request->institution_name;
        $education->institution_web = $request->institution_web;
        $education->degree_description = $request->degree_description;
        $education->user_id = auth()->id();
        $education->save();

        return $education;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function show(Education $education)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function edit(Education $education)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([
            'start_year' => 'required',
            'finish_year' => 'required',
            'degree' => 'required',
            'institution_name' => 'required',
        ]);

        $education = Education::find($id);
        $education->start_year = $request->start_year;
        $education->start_month = $request->start_month;
        $education->finish_year = $request->finish_year;
        $education->finish_month = $request->finish_month;
        $education->degree = $request->degree;
        $education->institution_name = $request->institution_name;
        $education->institution_web = $request->institution_web;
        $education->degree_description = $request->degree_description;
        $education->user_id = auth()->id();
        $education->save();

        return $education;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //
        $education = Education::find($id);
        $education->delete();
    }
}
