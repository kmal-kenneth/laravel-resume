<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->year('start_year');
            $table->text('start_month')->nullable();
            $table->year('finish_year');
            $table->text('finish_month')->nullable();
            $table->text('position');
            $table->text('company_name');
            $table->text('company_web')->nullable();
            $table->text('job_description')->nullable();
            $table->timestamps();

            //Llaves foráneas
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
