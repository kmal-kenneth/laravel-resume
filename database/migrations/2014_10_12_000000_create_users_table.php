<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->text('email')->unique();
            $table->text('password');
            $table->text('first_name');
            $table->text('last_name');
            $table->text('profession')->nullable();
            $table->string('avatar')->default('user.jpg');
            $table->text('profile')->nullable();
            $table->text('phone_number')->nullable();
            $table->text('web')->nullable();
            $table->text('git_user')->nullable();
            $table->text('address')->nullable();
            $table->text('city')->nullable();
            $table->text('country')->nullable();
            $table->text('knowledge')->nullable();
            $table->text('birth_year')->nullable();
            $table->text('place_of_birth')->nullable();
            $table->text('citizen_id')->nullable();
            $table->text('driver_license')->nullable();

            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
