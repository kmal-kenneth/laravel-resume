const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.pug$/,
                oneOf: [
                    {
                        resourceQuery: /^\?vue/,
                        use: ["pug-plain-loader"],
                    },
                    {
                        use: ["raw-loader", "pug-plain-loader"],
                    },
                ],
            },
        ],
    },
});

mix.ts("resources/js/app.ts", "public/js");
//    .sass('resources/sass/app.scss', 'public/css');

const tailwindcss = require("tailwindcss");

mix.sass("resources/sass/app.scss", "public/css").options({
    processCssUrls: false,
    postCss: [tailwindcss("./tailwind.config.js")],
});

mix.browserSync("http://127.0.0.1:8000/").disableNotifications();

if (mix.inProduction()) {
    mix.version();
}
