<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 * wizard personal
 */

Route::get('/wizard/personal', 'UserController@wizard')->name(
    'wizard.personal'
);

Route::patch('wizard/{wizard}', 'UserController@update')->name('wizard.update');

Route::get('/wizard/photo', 'UserController@wizard_photo')->name(
    'wizard.photo'
);

Route::post('/wizard/photo', 'UserController@update_avatar')->name(
    'wizard.photo'
);

/**
 * wizard experience
 */
Route::get('wizard/experience', 'ExperienceController@wizard')->name(
    'wizard.experience'
);

Route::post('wizard/experience', 'ExperienceController@store')->name(
    'wizard.experience'
);

Route::delete('wizard/experience/{experience}', 'ExperienceController@destroy');

Route::patch('wizard/experience/{experience}', 'ExperienceController@update');

/**
 * wizard education
 */

Route::get('wizard/education', 'EducationController@wizard')->name(
    'wizard.education'
);

Route::post('wizard/education', 'EducationController@store')->name(
    'wizard.education'
);

Route::delete('wizard/education/{education}', 'EducationController@destroy');

Route::patch('wizard/education/{education}', 'EducationController@update');

/**
 * wizard templates
 */

/**
 * wizard education
 */

Route::get('wizard/template', 'TemplateController@wizard')->name(
    'wizard.template'
);
Route::post('wizard/template', 'TemplateController@download')->name(
    'wizard.template'
);

Route::view('template1', 'template1')->middleware('auth');
Route::view('template2', 'template2')->middleware('auth');
Route::view('template3', 'template3')->middleware('auth');
Route::view('template4', 'template4')->middleware('auth');
Route::view('template5', 'template5')->middleware('auth');
Route::view('template6', 'template6')->middleware('auth');
Route::view('template7', 'template7')->middleware('auth');
Route::view('template8', 'template8')->middleware('auth');
